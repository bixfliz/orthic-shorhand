# Orthic Shorhand

## This repo

* Orthic Font that I created using FontForge.  (the sfd file is the project file)
* SVG file that I created by tracing the 'Specimen of Fully-Written Style' example.  Created with Skedio Android app.
* Anki dek that I use for learning.

## Helpful Links

* https://orthic.shorthand.fun/
* https://www.reddit.com/r/orthic/

